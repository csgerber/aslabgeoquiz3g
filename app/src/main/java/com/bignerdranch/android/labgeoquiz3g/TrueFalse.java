package com.bignerdranch.android.labgeoquiz3g;

//this class will be used as our model. It has two members:
// an int that represents a pointer to a (resourced) string
// a boolean that just let's us know if the answer is true (if it's not true, then it must be false)
public class TrueFalse {
    // this variable will hold a resource ID for a string
    private int mQuestion;
    private boolean mTrueQuestion;

    //constructor
    public TrueFalse(int question, boolean trueQuestion) {
        mQuestion = question;
        mTrueQuestion = trueQuestion;
    }

    //getters/setters
    public int getQuestion() {
        return mQuestion;
    }

    public void setQuestion(int question) {
        mQuestion = question;
    }

    public boolean isTrueQuestion() {
        return mTrueQuestion;
    }

    public void setTrueQuestion(boolean trueQuestion) {
        mTrueQuestion = trueQuestion;
    }

}
